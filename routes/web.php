<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.app');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('events', 'EventController@index')->name('events.index');
Route::post('events', 'EventController@addEvent')->name('events.add');
//Route::post('events', 'EventController@destroy')->name('events.delete')
//Route::post('/slideshows/create', [
//        'uses' => 'SlideshowsController@create',
//        'as' => 'slideshows.create'
//    ]);

//Route::delete('slideshows/{id}', 'SlideshowsController@destroy');
//Route::patch('slideshows/update/{id}', 'SlideshowsController@update');
//Route::post('slideshows/edit/{id}', 'SlideshowsController@edit');
//Route::get('slideshows', 'SlideshowsController@calendar');