<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use App\Event;
use App\Tag;
use DB;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use App\Http\Controllers\Controller;
use Auth;
use Validator;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $events = Event::get();
        $event_list = [];
        foreach ($events as $key => $event) {
            $event_list[] = Calendar::event(
                $event->thing_name,
                true,
                new \DateTime($event->start_at),
                new \DateTime($event->end_at.' +1 day')
            );
        }
        $tags = Tag::all();
        $calendar = Calendar::addEvents($event_list); 
        return view('Event', compact('calendar'), ["events"=>$events] )->with('data', $tags);
    }

    public function addEvent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'thing_name' => 'required',
            'start_at' => 'required',
            'end_at' => 'required'
        ]);

        if ($validator->fails()) {
            \Session::flash('warnning','Please enter the valid details');
            return Redirect::to('/events')->withInput()->withErrors($validator);
        }

        $event = new Event;
        $event->username = $request['username'];
        $event->tag_id = $request['tag_id'];
        $event->thing_name = $request['thing_name'];
        $event->start_at = $request['start_at'];
        $event->end_at = $request['end_at'];
        $event->time = $request['time'];
        $event->save();

        \Session::flash('success','Event added successfully.');
        return Redirect::to('events');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    //public function create(Request $request)
    //{

        /*$input = Input::all();*/
    //    $events = new \App\Event;
    //    $event->username = $request->username;
    //    $events->tag_id = $request->tag_id;
    //    $events->thing_name = $request->thing_name;
    //    $events->start_at = $request->start_at;
    //    $events->end_at = $request->end_at;
    //    $events->time = $request->time;
    //    $events->save();
    //    return redirect()->back();//
    //}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Show the form for editing the specified resource.
     * 
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function edit(Request $request,$id)
    //{
    //    $events = Event::find($id);
    //    $events->tag_id = $request->tag_id;
    //    $events->thing_name = $request->thing_name;
    //    $events->start_at = $request->start_at;
    //    $events->time = $request->time;
    //    $events->save();
    //    //slideshow::where('id', $slideshows)->update($request->all());
    //    return redirect()->back();;
    //}//

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function update(Request $request, $id)
    //{

     //   Event::where('id', $events)->update($request->all());

     //   return redirect('home');
    //}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $events = Event::findOrFail($id);
        $events->delete();
        return \Redirect::to('events');
    }
}
