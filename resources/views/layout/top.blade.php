<!DOCTYPE html>
<html>
<head>
<style>
.navbar-light {
  font-family: 'Microsoft JhengHei', Times, serif;
  font-size: 15px;
  background-color: #126398;
}
#topmenu a {
  color: white;
}
#topmenu a:hover {
  color: pink;
  background-color: #126398;
  text-decoration: none;
}
</style>
</head>
<body>
<nav class="navbar navbar-light">
  <div class="container-fluid">
    <ul class="nav navbar-nav" id="topmenu">
      <li><a href="{{ URL::asset('home') }}">前台管理</a></li>
      @if (Route::has('login'))
          @if (Auth::check())
              <li style="font-size: 30px;color: white;">｜</li>
              <li>
                                              <a href="{{ route('logout') }}"
                                                  onclick="event.preventDefault();
                                                           document.getElementById('logout-form').submit();">
                                                  Logout
                                              </a>

                                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                  {{ csrf_field() }}
                                              </form>
              </li>
          @endif
      @endif
    </ul>
  </div>
</nav>
</body>
</html>