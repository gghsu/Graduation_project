@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
<link href='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.3/jquery-ui.css' rel='stylesheet' />
@endsection
 
@section('content')
        <div class="container">
 
            <div class="panel panel-primary">
 
             <div class="panel-heading">Add Panel</div>
 
              <div class="panel-body">    
 
                   {!! Form::open(array('route' => 'events.add','method'=>'POST','files'=>'true')) !!}
                    <div class="row">
                       <div class="col-xs-12 col-sm-12 col-md-12">
                          @if (Session::has('success'))
                             <div class="alert alert-success">{{ Session::get('success') }}</div>
                          @elseif (Session::has('warnning'))
                              <div class="alert alert-danger">{{ Session::get('warnning') }}</div>
                          @endif
 
                      </div>

                      <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            {!! Form::label('username','username:') !!}
                            <div class="">
                            <input type="text" name="username" required="required" value="{{Auth::user()->name}}" readonly/>
                            {!! $errors->first('username', '<p class="alert alert-danger">:message</p>') !!}
                            </div>
                        </div>
                      </div>

                      <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            {!! Form::label('thing_name','Object') !!}
                            <div class="">
                            {!! Form::text('thing_name', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('thing_name', '<p class="alert alert-danger">:message</p>') !!}
                            </div>
                        </div>
                      </div>
 
                      <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="form-group">
                          {!! Form::label('start_at','Start Date:') !!}
                          <div class="">
                          {!! Form::date('start_at', null, ['class' => 'form-control']) !!}
                          {!! $errors->first('start_at', '<p class="alert alert-danger">:message</p>') !!}
                          </div>
                        </div>
                      </div>
 
                      <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="form-group">
                          {!! Form::label('end_at','End Date:') !!}
                          <div class="">
                          {!! Form::date('end_at', null, ['class' => 'form-control']) !!}
                          {!! $errors->first('end_at', '<p class="alert alert-danger">:message</p>') !!}
                          </div>
                        </div>
                      </div>

                      <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            {!! Form::label('tag_id','tag id:') !!}
                            <div class="">
                            <select name="tag_id">
                            @foreach( $data as $tag ) 
                            <option value=" <?php echo  $tag->rfid_tag; ?>" > <?php echo $tag->rfid_tag?> </option>
                            @endforeach
                            </select>
                            {!! $errors->first('tag_id', '<p class="alert alert-danger">:message</p>') !!}
                            </div>
                        </div>
                      </div>

                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <div class="form-group">
                          {!! Form::label('time','time:') !!}
                          <div class="">
                            <select name="time">
                            <option value="morning" >morning</option>
                            <option value="afternoon" >afternoon</option>
                            <option value="night" >night</option>
                            </select>
                          {!! $errors->first('time', '<p class="alert alert-danger">:message</p>') !!}
                          </div>
                        </div>
                      </div>
 
                      <div class="col-xs-1 col-sm-1 col-md-1 text-center"> &nbsp;<br/>
                      {!! Form::submit('Add Event',['class'=>'btn btn-primary']) !!}
                      </div>
                    </div>
                   {!! Form::close() !!}
 
             </div>
 
            </div>
 
            <div class="panel panel-primary">
              <div class="panel-heading">Calendar</div>
              <div class="panel-body" >
                  <div id="calendar"></div> 
            </div>

            <div id="calendarModal" class="modal fade">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                          <h4 id="modalTitle" class="modal-title"></h4>
                      </div>
                      <div id="modalBody" class="modal-body"> </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <input type="hidden" id="ModalId"/>
                          <button type="button" class="btn red-intense Recusar">delete</button></button>
                      </div>
                  </div>
              </div>
              </div>
            </div>
@endsection
 
@section('script')
<!-- Scripts -->
<script src="http://code.jquery.com/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
        $(document).ready(function() {
        // page is now ready, initialize the calendar...
        $('#calendar').fullCalendar({
            // put your options and callbacks here 
            editable : true,
            startEditable: true,
            selectable: true,
            selectHelper: true,
            //events: source,
            header: {
                left: '',
                center: 'prev title next',
                right: ''
            },
            events : [
                @foreach($events as $event)
                {
                    title : '{{ $event->username }}',
                    id : '{{ $event->id }}',
                    thing_name : '{{ $event->thing_name}}',
                    tag_id :  '{{ $event->tag_id}}',
                    start : '{{ $event->start_at }}',
                    end : '{{ $event->end_at }}',
                },
                @endforeach
            ],
            //eventClick: function(event){
            //  $('#calendar').fullCalendar('removeEvents',event.id);
            //}
            eventClick:  function(events) {
                $('#modalTitle').html("Username : " + events.title);
                $('#modalBody').html("Thing Name : " + events.thing_name + '<br>' + "RFID Tag : " + events.tag_id + '<br>');
                $('#calendarModal').modal('show');
            },
        });
        $('.Recusar').click(function (events) {
           var id = $('#ModalId').val();
           $('#calendar').fullCalendar('removeEvents', id);
           $('#calendarModal').modal('hide');
        });

    });
</script>
@endsection