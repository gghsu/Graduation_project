@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
@endsection

@section('content')
    <h1 style="text-align: center;">Tag管理</h1>
                        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#SlideshowModal" style="margin-left: 47%;">新增Tag</button>
                        <!-- Modal -->
                        <div class="modal fade" id="SlideshowModal" role="dialog">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">新增Tag</h4>
                                </div>
                                <div class="modal-body">
                                  <form id="slideshow_form" method="post" action="{{ url('/slideshows/create') }}">
                                    {!! csrf_field() !!}
                                    你是誰? <input type="text" name="username" required="required" value="{{Auth::user()->name}}" readonly/><br>
                                    Tag_ID: <input type="text" name="tag_id" required="required" /><br>
                                    物品名字: <input type="text" name="thing_name" required="required" /><br>
                                    開始日期: <input type="date" name="start_at" required="required" >
                                    時間: 
                                    <select name="time" required="required">
                                        <option value="morning">早上</option>
                                        <option value="afternoon">下午</option>
                                        <option value="night">晚上</option>
                                      </select>
                                      <br><br>
                                    <input type="submit" value="Submit">
                                    </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="container">
                            <table class="fancytable" border="2">
                            　<tr class="headerrow">
                            　<td>ID</td>
                              <td>擁有者</td>
                              <td>Tag_ID</td>
                            　<td>物件名字</td>
                              <td>提醒日期</td>
                              <td>提醒時間</td>
                              <td>建立時間</td>
                              <td>編輯</td>
                              <td>刪除</td>
                            　</tr>
                                @foreach ($slideshows as $slideshows)
                                  <tr class="datarowodd">
                                  <td>{{ $slideshows->id }}</td>
                                  <td>{{ $slideshows->username }}</td>
                                  <td>{{ $slideshows->tag_id }}</td>
                                　<td>{{ $slideshows->thing_name }}</td>
                                  <td>{{ $slideshows->start_at }}</td>
                                  <td>{{ $slideshows->time }}</td>
                                  <td>{{ $slideshows->created_at }}</td>
                                  <td>
                                    <button type="button" data-toggle="modal" data-target="#SlideshowModaledit{{ $slideshows->id }}" >編輯</button>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="SlideshowModaledit{{ $slideshows->id }}" role="dialog">
                                                        <div class="modal-dialog modal-lg">
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                              <h4 class="modal-title">編輯Tag</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                              <form id="slideshow_form" method="POST" action="/slideshows/edit/{{ $slideshows->id }}" >
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                你是誰? <input type="text" name="username" required="required" value="{{Auth::user()->name}}" readonly/><br>
                                                                Tag_ID: <input type="text" name="tag_id" required="required" value="{{ $slideshows->tag_id }}"/><br>
                                                                物品名字: <input type="text" name="thing_name" required="required" value="{{ $slideshows->thing_name }}"/><br>
                                                                日期: <input type="date" name="start_at" required="required" value="{{ $slideshows->start_at }}">
                                                                時間: 
                                                                <select name="time" required="required" value="{{ $slideshows->time }}">
                                                                    <option value="morning">早上</option>
                                                                    <option value="afternoon">下午</option>
                                                                    <option value="night">晚上</option>
                                                                  </select>
                                                                  <br><br>
                                                              
                                                                <input type="submit" value="Submit">
                                                                </form>
                                                            </div>
                                                            <div class="modal-footer">
                                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                          </div>
                                                        </div>
                                                    </div>
                                  </td>
                                  <td>
                                   <form action="/slideshows/{{ $slideshows->id }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button>刪除</button>
                                    </form>
                                  </td>
                                  </tr>     
                                @endforeach
                              </table>
                            </div>
</div>
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    {!! $calendar->script() !!}
    <script>
        $(document).ready(function(){
            $(".nav-list a").click(function(){
                $(this).tab('show');
            });
        });
    </script>
 @endsection

@endsection